# ImageCropper

> 一个用于vue的图片裁剪工具

> HellowWorld 是使用示例； ImageCropper是组件代码，放进项目即可使用

> 组件源码来源于花裤衩大神的后台模板集成方案，我只是对常用功能做了说明和简单优化

> https://juejin.im/post/59097cd7a22b9d0065fb61d2

> 附上链接，以示对大神的敬意

## Build Setup（启动项目查看效果）

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

```

## Introduction

``` bash
# 在页面中引入组件
import ImageCropper from '@/components/ImageCropper/index'

# 编写模板
  <image-cropper
    :value="cropperShow"
    @close="cropperShow=false"
    :url="uploadUrl"
    :field="fieldName"
    :noCircle="noCircle"
    :noSquare="noSquare"
    :width="width"
    :height="height"
    :maxSize="maxSize"
    @crop-success="getPicFunc"
    @crop-upload-success="successFunc"
  ></image-cropper>

# 设置参数 / 方法
  data () {
    return {
      // 控制组件显示
      cropperShow: false,
      // 上传地址
      uploadUrl: '/kukacms/visitor/picUpload.htm?type=2',
      // 上传文件名
      fieldName: 'files',
      // 预览圆形图片 false为显示
      noCircle: true,
      // 预览方形图片 false为显示
      noSquare: false,
      // 裁剪图片宽高(即所需要图片的宽高)
      width: 300,
      height: 150,
      // 大小限制
      maxSize: 10240,
    }
  },
  methods: {
    // 图片截取后调用，用于获取图片
    getPicFunc (data) {
      console.log(data)
    },
    // 图片上传成功后执行函数
    successFunc (data) {
      console.log(data)
    }
  },

```